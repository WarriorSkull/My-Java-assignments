package Practice;


import java.io.*;

public class ContactManagerv1 {
    //global variables
    static int counter = 0;
    //method to get user's input.
    static BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
    //array to hold names
    static String[] names = new String[100];
    //array to hold numbers
    static String[] numbers = new String[100];
    //array to hold addresses
    static String[] addresses = new String[100];
    //array to hold emails
    static String[] emails = new String[100];


    /****************
     * name: main
     * date: 31-12-2020
     * Description: Displays the Contact list menu for the user to choose from.
     * input/output: takes user's integer input for choice of the option to choose from the menu.
     ****************/
    public static void main(String[] args) throws IOException {
        //declared and initialised counter to -1.
        int choice = -1;
        // keep running the program until 7 is entered
        while (choice != 7) {
            System.out.println("Main Menu");
            System.out.println(
                    "1. Add Entry \n" +
                    "2. Modify Entry\n" +
                    "3. Delete Entry\n" +
                    "4. View All Entry\n" +
                    "5. Save Book\n" +
                    "6. load Book\n" +
                    "7. Quit the Menu\n"
            );
            // enclose input try/catch block in case of an error.
            try {
                // prompts the user for integer value for options
                System.out.println("Enter a number from 1-7!");
                choice = Integer.parseInt(input.readLine());
            }catch (Exception e){
                System.out.println("Please type only numbers");
                //set choice to zero so next time when the user enter a wrong letter/number no option is selected.
                choice = 0;
            }
            // according to user's choice run different methods.
            switch (choice) {
                case 1:
                    addEntry();
                    break;
                case 2:
                    modifyEntry();
                    break;
                case 3:
                    deleteEntry();
                    break;
                case 4:
                    viewAllEntries();
                    break;
                case 5:
                    saveBook();
                    break;
                case 6:
                    loadBook();
                    break;
                case 7:
                    System.out.println("Quiting the Menu");
                    break;
                default:
                    System.out.println("OOPS! Wrong number chosen");
                    break;
            }
        }
    }
    /****************
     * name: addEntry
     * date: 31-12-2020
     * Description: add the user defined record into the arrays when ever the command is selected
                    until the array size is full.
     * input/output: takes user String input for name, number, address and email and store in the desired arrays.
     ****************/
    public static void addEntry() throws IOException {

        //check the counter variable
        if (counter < 100) {
            //prompt the user for information
            System.out.println("Please enter your Contact name: ");
            names[counter] = input.readLine().toUpperCase();

            System.out.println("Please enter your Contact number: ");
            numbers[counter] = input.readLine();

            System.out.println("Please enter your Contact location : ");
            addresses[counter] = input.readLine();

            System.out.println("Please enter your Contact email address: ");
            emails[counter] = input.readLine();

            //increment the counter
            counter++;
        } else {
            System.out.println("You can only save 100 contacts! Contact list full!");
        }
    }


    /****************
     * name: modifyEntry
     * date: 31-12-2020
     * Description: Matches the previous entered record and modifies the record that are already stored in the array.
     *              Gives an error message if record is not available.
     * input/output: takes input from the user to match with the previous record and allows to reenter the details.
     ****************/
    public static void modifyEntry() throws IOException {
        //flag is used to mark a spot in the array
        int flag = 0;
        //found indicates if the search key has a match
        boolean found = false;
        //variable to hold search criteria
        System.out.println("Please provide the name of the contact to modify its entry: ");
        String searchName = input.readLine().toUpperCase();
        // enclose in try/catch block in case of an error.
        try {
            // loop to search for the record name in each index
            for (int i = 0; i < names.length; i++) {
                //compare the string
                if (names[i].compareTo(searchName) == 0) {
                    flag = i;
                    found = true;

                }
        }
            // catch all other exceptions and ignore them.
        }catch (Exception ignored){

        }
        // if record not found , display an error.
        if (!found) {
            System.out.println("Entry not found in the contacts list");
            // if found , then ask user for the new modification.
        } else {
            System.out.println("What name would you like for the contact: ");
            names[flag] = input.readLine().toUpperCase();

            System.out.println("What contact number would you like to add: ");
            numbers[flag] = input.readLine();

            System.out.println("What contact address would you like to add: ");
            addresses[flag] = input.readLine();

            System.out.println("What contact email would you like to add: ");
            emails[flag] = input.readLine();
        }
    }

    /****************
     * name: deleteEntry
     * date: 31-12-2020
     * Description: Deletes the record specified by the user from the array if it is found in the lists.
     * input/output: takes input from the user to match with the previous record and deletes the whole record of that
     *              individual.
     ****************/
    public static void deleteEntry() throws IOException {
        // flag for if the record is not found
        int flag = -1;
       // ask user for the record to search
        System.out.println("Please provide the name of the contact to delete its entry: ");
        String searchName2 = input.readLine().toUpperCase();


        // enclose input try/catch block input case of an error.
        try {
            // loop to search for the record name input each index
            for (int i = 0; i < counter; i++) {
                if (names[i].compareTo(searchName2) == 0) {
                    flag = i;

                }
            }
            // catch all other exceptions and ignore them
        } catch (Exception ignored) {
        }
        //record was not found
        if (flag == -1) {
            System.out.println("Contact Name  NOT Found");
        } else { //record was found, flag is a positive index position
            //if the record is the last record, simply remove it and decrement the counter
            if (flag == counter - 1) {
                System.out.println("Contact Deleted!");
                names[flag] = "";
                numbers[flag] = "";
                addresses[flag] = "";
                emails[flag] = "";
                counter--;
            }else {
                //record is not the last record, shift all later records down
                for (int i = flag; i < counter-1; i++){
                    names[i] = names[i + 1];
                    numbers[i] = numbers[i + 1];
                    addresses[i]= addresses[i +1];
                    emails[i] = addresses[i+1];

                }
                counter --;
                System.out.println(counter);
                System.out.println("Contact Deleted! "+ counter + " contacts left in the list.");
                // at the last record make it empty
                names[counter] = "";
                numbers[counter] = "";
                addresses[counter] = "";
                emails[counter] = "";
        }
    }
    }

    /****************
     * name: viewAllEntries
     * date: 31-12-2020
     * Description: Display lists of the entire names, numbers, email and addresses arrays to standard output
     * input/output: NA
     ****************/
        public static void viewAllEntries () throws NullPointerException {
            for (int i = 0; i < names.length; i++) {
                // Only shows the contacts that are filled input.
                if (names[i] != null && !names[i].equals("")) {
                    System.out.println("Name: " + names[i] + "| Number: " +
                            numbers[i] + "| Address: " + addresses[i] + "| Email: " + emails[i]);
                }

            }
            System.out.println("Total entries: " + counter);
        }

    /************
     * name: saveBook
     * date: 31-12-2020
     * Description: Saves a csv file which contains all the contacts list of the user.
     * input/output: NA
     ***********/
        public static void saveBook () throws IOException {
            //Create File output
            PrintWriter output;
            output = new PrintWriter(new FileWriter("Contacts.csv"));
            //loop through out  all the arrays outputting to a file
            for (int i = 0; i < counter; i++) {
                output.println(names[i] + "," + numbers[i]+","+ addresses[i] +"," + emails[i]);
            }
            System.out.println("Book Saved!");
            //close file!
            output.close();
        }

    /****************
     * name: loadBook
     * date: 31-12-2020
     * Description: loads the file that was previously stored by the user which contains all the contacts.
     * input/output: input reads the data that is stored in the file.
     ****************/
    public static void loadBook() {
        //String variable line to hold the input
        String line;
        //Buffered reader ot read input
        BufferedReader in;
        //delimited parts of the line
        String[] tokens;
        //The delimiter is a comma .
        String delims = ",";
        int i = 0;
        //read names from Contacts.csv, use to initialize array
//		//enclose input a try...catch block incase of an exception
        try {

			//initialize the reader for Contacts.csv;
			in = new BufferedReader(new FileReader("Contacts.csv"));

			line = in.readLine();

			//populate the array with all the names, numbers, addresses and emails
			while ( i < 100 && line != null){
                //read the saved data
				tokens = line.split(delims);
				names[i] = tokens[0];
                numbers[i] = tokens[1];
                addresses[i] = tokens[2];
                emails[i] = tokens[3];

				line = in.readLine();


				i++;

			}
            System.out.println("Book Loaded!");

			//catch if the file is not found
		} catch (FileNotFoundException e) {
			System.out.println("File not found");

			//catch if there is an error input I/O
		} catch (IOException e) {
			System.out.println("IO Error");

			// catch all other exceptions and ignore them.
		}catch(Exception ignored){
        }
        // set counter to i to indicate the number lines already filled
        counter = i;
    }

}